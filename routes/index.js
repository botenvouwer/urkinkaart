var express = require('express');
var router = express.Router();

const { Pool } = require('pg');
const connectionString = 'postgresql://test:test@localhost/urkinkaart';

const pool = new Pool({
    connectionString: connectionString,
});

/* GET home page. */
router.get('/', function(req, res, next) {

    pool.query('SELECT * FROM wijken;', (err, result) => {
        //console.log(err, res);

        res.render('index', { title: 'Doe eens georefereren', rows: result.rows });
    });
});

module.exports = router;
